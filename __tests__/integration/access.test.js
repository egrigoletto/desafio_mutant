const users = require('../../controllers/users');
const request = require('supertest');
const app = require('../../src/app');

describe('Integration', () => {
  it('Should return a 200 status when called a default route', async () => {
    const response = await request (app)
    .get('/');
    
    expect(response.status).toBe(200)
  })

  it('Should return a 404 status when called another route than default', async () => {
    const response = await request (app)
    .get('/tests');

    expect(response.status).toBe(404)
  })
})
