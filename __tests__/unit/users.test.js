const users = require('../../controllers/users');
const request = require('supertest');
const app = require('../../src/app');
const arraySort = require('array-sort');
const { response } = require('express');


describe('User data validation', () => {
  it('Should validate if all users have a website into your data and this website is not a empty', async () => {
    const response = await request (app)
    .get('/test');
    let validationFlag = true;

    response.body.users.map(user => {
      if (!user.website || user.website === '')
        validationFlag = false
    })

    expect(validationFlag).toBe(true);
  })

  it('Should verify if the user list based in name, email and company name is sorted by alphabetic order based in "name" field', async () => {
    let responseApp = await request (app)
    .get('/test');
    let responseApi = await request ('https://jsonplaceholder.typicode.com/users')
    .get('/');
    let usersApiArr = [];
    let usersAppArr = [];

    responseApp = responseApp.body.users;
    responseApi = arraySort(responseApi.body, 'name', { reverse: false });

    responseApi.map(userApi => {
      if (userApi.address.suite.toUpperCase().search("SUITE") !== -1) {
        usersApiArr.push({
          name: userApi.name,
          email: userApi.email, 
          company: userApi.company.name,
        });
      }
    });    

    responseApp.map(userApp => {
      usersAppArr.push({
        name: userApp.name, 
        email: userApp.email,
        company: userApp.company,
      });
    });

    expect(usersApiArr).toEqual(usersAppArr)
  })

  it('Should verify if the app returns all users with address with "Suite" at "suite" address key', async () => {
    let responseApp = await request (app)
    .get('/test');
    let responseApi = await request ('https://jsonplaceholder.typicode.com/users')
    .get('/');
    let validationArray = [];
    
    responseApp = responseApp.body.users;
    responseApi = arraySort(responseApi.body, 'name', { reverse: false });

    responseApi.map(userApi => {
      if (userApi.address.suite.toUpperCase().search("SUITE") !== -1)
        validationArray.push(userApi);
    })

    expect(validationArray.length).toEqual(responseApp.length)
  })

})

