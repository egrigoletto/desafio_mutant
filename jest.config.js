const nodemon = require("nodemon");

module.exports = {
  bail: true,
  clearMocks: true,
  testEnvironment: "node",
  testMatch: [
    "**/__tests__/**/*.test.js?(x)",
  ],
}