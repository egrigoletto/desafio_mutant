# Desafio Mutant

Este reprositório contém uma prova feita para um processo seletivo, ela pegará uma lista de usuários disponível em https://jsonplaceholder.typicode.com/users e irá renderizar na raiz uma lista baseado nos parâmetros abaixo:

* Devem ser mostrados em ordem alfabética os usuários .
* Os dados a ser informados sobre os usuários são, nome, email, nome da empresa onde trabalha e website
* Apenas usuários que tenham em seu endereço no campo "suite" a palavra "Suite" serão listados

Há uma rota principal na raiz da página que vai renderizar os as informações baseado nos parâmetros acima e uma rota exclusiva para testes.
Os testes cobrem dois pontos:

 - integração
 - condições para listagem baseado nos parâmetros acima

## Como usar


### Com Docker

Com o docker instalado em sua máquina execute o comando abaixo

	- docker-compose up

Com a máquina iniciada aparecerá uma instrução escrito "Servidor Rodando". Em casos onde há uma máquina windows rodando a docker toolbox a mensagem pode não aparecer.
Para visualizar acesse os endereços abaixo:

	 - http://localhost - para máquinas linux ou instalalções de com "Docker for Windows"
	 - http://<endereço_ip_da_máquina_docker> - para instalações windows com "Docker Toolbox"

Com isso, uma lista será renderizada na tela 

Para executar os testes deve-se acessar o container onde está o instância do containter, caso não saiba, digite

	- docker ps 

Copie o ID do container e inclua-o no comando abaixo:

	- docker exec -it <container-id> bash

Com o bash aberto insira o seguinte comando:

	- npm run test

### Com NodeJs

Execute o comando abaixo com o node instalado na máquina, execute o comando abaixo:

	- npm run start

Para executar os testes, execute o comando abaixo:

	- npm run test