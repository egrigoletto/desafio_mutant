const axios = require('axios');
const arraySort = require('array-sort');

module.exports.list = async function list() {
  const responseData = await axios.get('https://jsonplaceholder.typicode.com/users');
  let users = [];
  responseData.data.map(user => {
    if (user.address.suite.toUpperCase().search("SUITE") !== -1) {
      let addUser = {
            id: user.id,
            name: user.name,
            email: user.email,
            company: user.company.name,
            website: user.website,
      }
      users.push(addUser);
    }
  })
  const responseJson = {
    message: 'Operação executada com sucesso',
    users: arraySort(users, 'name', { reverse: false }),
  };
  return responseJson;
}
