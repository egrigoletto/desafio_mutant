const routes = require('express').Router();
const users = require('../controllers/users')

routes.get('/', async (req, res) => {
  const response = await users.list()
  res
  .status(200)
  .render('./home/index', {users: response.users})
})

routes.get('/test', async (req, res) => {
  const response = await users.list()
  res
  .status(200)
  .send(response)
})

module.exports = routes;